import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  HashRouter
} from "react-router-dom";
import  Home from './Components/Home'
import  Agreement from './Components/Pages/Agreement'
import  EnterAR from './Components/Pages/EnterAR'
import  FaceScan from './Components/Pages/FaceScan'
import  UploadIdFront from './Components/Pages/UploadIdFront'
import  UploadIdBack from './Components/Pages/UploadIdBack'
import  SocialSecurityId from './Components/Pages/SocialSecurityId'
import  SocialSecurityIdManualEntry from './Components/Pages/SocialSecurityIdManualEntry'
import  Documents from './Components/Pages/Documents'
import  PesonalInformationForm from './Components/Pages/PesonalInformationForm'
import  FinishedVerification from './Components/Pages/FinishedVerification'
import  Dashboard from './Components/Pages/Dashboard'
import { createBrowserHistory } from "history";
import './App.css';
import 'materialize-css/dist/css/materialize.min.css';
import Face from './Components/login/Face';
import Int from './Components/Pages/Int';


const appHistory = createBrowserHistory();
function App() {

    useEffect(() => {
     
      });
  return (
    <div >
        {/* <Router basename="/FactualApp"></Router> */}
       <Router history={appHistory}  basename="/FactualApp">
       <Switch>
        <Route exact path="/">
            <Home />
        </Route>
        <Route exact path="/Agreement">
            <Agreement />
        </Route>
        <Route exact path="/EnterAR">
            <EnterAR />
        </Route>
        <Route exact path="/FaceScan">
            <FaceScan />
    
        </Route>
        <Route exact path="/UploadIdFront">
            <UploadIdFront />
        </Route>
       
        <Route exact path="/UploadIdBack">
            <UploadIdBack />
        </Route>
        <Route exact path="/SocialSecurityId">
            <SocialSecurityId />
        </Route>
        <Route exact path="/SocialSecurityIdManualEntry">
            <SocialSecurityIdManualEntry />
        </Route>
        <Route exact path="/Documents">
            <Documents />
        </Route>
        <Route exact path="/PesonalInformationForm">
            <PesonalInformationForm />
        </Route>
        <Route exact path="/FinishedVerification">
            <FinishedVerification />
        </Route>
        <Route exact path="/Dashboard">
            <Dashboard />
        </Route>
        <Route exact path="/Face">
            <Face />
        </Route>
        <Route exact path="/Int">
            <Int />
        </Route>
      </Switch>
      </Router>
    </div>
  );
}

export default App;
