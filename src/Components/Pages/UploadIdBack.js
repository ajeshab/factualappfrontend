import React, { Component, useState, useRef} from 'react';
import Header from '../HeaderInner';
import Footer from '../FooterInner';
import axios from 'axios';
import Config from '../config';
import Webcam from "react-webcam";
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import IconButton from '@material-ui/core/IconButton';

const videoConstraints = {
  width: 250,
  height: 250,
  facingMode: "user"
};

export default function UploadIdBack() {

  const [image,setImage] = useState('')
  const webcamRef = useRef(null)

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImage(imageSrc);

    let payload={
      type:'id_back_image',
      back_image: image
    }
    axios.post(`${Config.apiRoot}/filesave`, {...payload}).then((data)=>{
      console.log('upload ssid successfull');
    }).catch((error)=>{
      console.log('upload ssid image error');
    })
    return;
  }, [webcamRef, setImage]);

  // const handleImg = (e) => {
  //   e.preventDefault()
  //   setFile(e.target.files[0])
  //   convertBase64(e.target.files[0]).then((data) => {
  //     setImage(data);
  //   }).catch(e => {
  //     console.log('convert base64 error');
  //   })
  // }

  // const convertBase64 = (file) => {
  //   return new Promise((resolve, reject) => {
  //     const fileReader = new FileReader()

  //     fileReader.onload = () => {
  //       resolve(fileReader.result);
  //     }
  //     fileReader.onerror = (error) => {
  //       reject(error);
  //     }
  //     fileReader.readAsDataURL(file);

  //   });
  // }


  // const handleUpload = (e) => {
  //   e.preventDefault();
  //   if(!image){alert('please select an image')};
  //   let payload = {
  //     type:'id_back_image',
  //     back_image: image
  //   }
  //   axios.post(`${Config.apiRoot}/filesave`, {...payload}).then((data)=>{
  //     console.log('upload back successfull');
  //   }).catch((error)=>{
  //     console.log('upload back image error');
  //   })
  // }
  return (
    <div className="wrapper">
      <Header next="SocialSecurityId" />
      <div class="main-wrap">
        <div class="sub-wrap">
          <div class="scanner-wrap">
            
            {/* <label for="file" ><div class="section" style={{}}>

              <img className={file ? "with-file" : null} src={file ? URL.createObjectURL(file) : require('../../assets/id.png')} />
              <input id="myInput"
                style={{ display: 'none' }}
                type={"file"}
                id="file"
                onChange={handleImg}

              />

            </div> </label> */}
            {/* <!-- </div> --> */}

            <Webcam
              audio={false}
              ref={webcamRef}
              screenshotFormat="image/jpeg"
              videoConstraints={videoConstraints}
            />
          </div>
          <label htmlFor="icon-button-file">
            <IconButton color="primary" aria-label="upload picture" component="span">
              <PhotoCamera onClick={capture} />
            </IconButton>
          </label>
          <div class="scanner-text-wrap">
            <button  style={{ color: " #c2c2c2", fontSize: "26px",border: 'none'  }}>UPLOAD ID BACK</button>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  )
}
