import React, { Component } from 'react';
import Header from '../HeaderInner';
import Footer from '../FooterInner';
export default class FaceScan extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header />
        <div class="main-wrap">
          <div class="sub-wrap" style={{ marginLeft: "0px" }}>
            <div class="scanner-wrap">
              <div class="section" style={{}}>
                <img src={require('../../assets/face_scan_web.png')} />
              </div>
              {/* <!-- </div> --> */}
            </div>
            <div class="scanner-text-wrap">
              <p style={{ color: " #c2c2c2", fontSize: "26px" }}>FACE SCAN</p>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}
