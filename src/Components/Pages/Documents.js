import React, { Component, useRef, useState } from 'react';
import Header from '../HeaderInner';
import Footer from '../FooterInner';
import axios from 'axios';
import Config from '../config';


export default function Documents() {
  const [file, setFile] = useState('')
  const [doc, setDoc] = useState('')

  const handleDocument = (e) => {
    e.preventDefault()
    setFile(e.target.files[0])
    convertBase64(e.target.files[0]).then((data) => {
      setDoc(data);
    }).catch(e => {
      console.log('convert document base64 error');
    })
  }

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader()

      fileReader.onload = () => {
        resolve(fileReader.result);
      }
      fileReader.onerror = (error) => {
        reject(error);
      }
      fileReader.readAsDataURL(file);

    });
  }


  const handleUpload = (e) => {
    e.preventDefault();
    if (!doc) { alert('please select an document') };

    var formData = new FormData();

    // bodyFormData.append('userName', 'Fred');
    // bodyFormData.append('image', doc);
    // const files = e.target.files;


    for (let i = 0; i < doc.length; i++) {
      formData.append(`images[${i}]`, doc[i])
    }



    axios({
      method: 'post',
      url: '',
      data: formData,
      headers: { 'Content-Type': 'multipart/form-data' }
    })
      .then(function (response) {
        //handle success
        console.log(response);
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  }
  return (
    <div className="wrapper">
      <Header next="PesonalInformationForm" />
      <div class="main-wrap">
        <div class="sub-wrap">
          <div class="scanner-wrap">
            <label for="file" ><div class="section" style={{}}>

              <img className={file ? "with-file" : null} src={file ? URL.createObjectURL(file) : require('../../assets/document.png')} />
              <input
                style={{ display: 'none' }}
                type={"file"}
                id="file"
                onChange={handleDocument}
                multiple

              />

            </div> </label>
          </div>
          <div class="scanner-text-wrap">
            <button onClick={handleUpload} style={{ color: " #c2c2c2", fontSize: "26px", border: 'none' }}>UPLOAD DOCUMENT</button>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  )
}
