import React, { Component } from 'react';
import Header from '../HeaderInner';
import Footer from '../FooterInner';
export default class FinishedVerification extends Component {
  render() {
    return (
        <div className="wrapper">
    
        <Header next="Dashboard"/> 
      <div class="main-wrap">
      <div class="sub-wrap">
      <div
          class=""
          style={{
            borderRadius: "10px",
            alignItems: "center",
            height: "350px",
            marginLeft: "16%",
            marginRight: "16%",
            textAlign: "center",
            backgroundColor: "#fff"
            }}
        >

          <div style={{padding:"6rem"}}>
            <div
              style={{
                textAlign:"center",
                fontSize: "22px",
                // font-family: adobe-clean, sans-serif;
                fontWeight: "400",
                color: "#000000"
              }}
            >
              Thank you for completing IDP verification.
            </div>
            <div
              style={{
                textAlign:"center",
                fontSize: "22px",
                // font-family: adobe-clean, sans-serif;
                fontWeight: "400",
                color: "#000000"
              }}
            >
              Please use AR# XXXXX for all future
            </div>
            <div
              style={{
                textAlign:"center",
                fontSize: "22px",
                // font-family: adobe-clean, sans-serif;
                fontWeight: "400",
                color: "#000000"
              }}
            >
              correspondance
            </div>
            <div class="section hide-on-small-only">
              <img src={require('../../assets/submit_tick.png')} />
            </div>
          </div>
        </div>
           </div>
           <Footer/>
            </div>
            </div>
    );
  }
}
