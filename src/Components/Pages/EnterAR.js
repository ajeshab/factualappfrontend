import React, { Component,useState } from 'react';
import { Link } from "react-router-dom";
import Header from '../HeaderInner';
import Footer from '../FooterInner';
import axios from 'axios';
import Config from '../config';



export default function () {

  const [ar, setAr] = useState('')
  
  const handleAr = (e) =>{
    e.preventDefault();
    if(!ar)alert('please enter the code ar#');
    let payload = {
      Ar_data:ar
    }
    axios.post(`${Config.apiRoot}`, {...payload}).then((data)=>{
      console.log('upload ar# successfull');
    }).catch((error)=>{
      console.log('upload ar#  error');
    })
  }


  return (
    <div className="wrapper">

      <Header next="UploadIdFront" />
      <div class="main-wrap ar-wrap">
        <div class="sub-wrap">
          <input
            id="email"
            type="email"
            class="validate"
            placeholder="Enter AR#"
            onChange={e => setAr(e.target.value)}
          />
          <Link onClick={handleAr} class="waves-effect waves-light btn-large col s6 offset-s3">ENTER</Link>

        </div>
        <Footer />
      </div>
    </div>
  )
}
