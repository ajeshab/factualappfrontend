import React, { Component, useState } from 'react';
import { Link } from "react-router-dom";
import Header from '../HeaderInner';
import Footer from '../FooterInner';
import Webcam from "react-webcam";
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import Config from '../config';


const videoConstraints = {
  width: 250,
  height: 250,
  facingMode: "user"
};
export default function SocialSecurityId() {

  const [image, setImage] = useState(null);
  const webcamRef = React.useRef(null);
  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImage(imageSrc);

    let payload={
      type:'id_back_image',
      back_image: image
    }
    axios.post(`${Config.apiRoot}/filesave`, {...payload}).then((data)=>{
      console.log('upload ssid successfull');
    }).catch((error)=>{
      console.log('upload ssid image error');
    })
    return;
  }, [webcamRef, setImage]);
  return (
    <div className="wrapper">

      <Header next="SocialSecurityIdManualEntry" />
      <div class="main-wrap ar-wrap scan-new-user">
        <div class="sub-wrap">
          <div class="scanner-wrap">
            {/* <div class="section" style={{}}> */}
            {/* <img src={require('../../assets/id.png')} /> */}

            <Webcam
              audio={false}
              ref={webcamRef}
              screenshotFormat="image/jpeg"
              videoConstraints={videoConstraints}
            />

            {image && (<img src={image}/>)}

            {/* </div> */}
          </div>
          <label htmlFor="icon-button-file">
            <IconButton color="primary" aria-label="upload picture" component="span">
              <PhotoCamera onClick={capture} />
            </IconButton>
          </label>
          <div class="scanner-text-wrap">
            <p style={{ color: "#c2c2c2", fontSize: "26px" }}>SOCIAL SECURITY</p>
          </div>
          <Link to="SocialSecurityIdManualEntry" class="waves-effect waves-light btn-large col s6 offset-s3">ENTER MANUALY</Link>
        </div>
        <Footer />
      </div>
    </div >
  )
}
