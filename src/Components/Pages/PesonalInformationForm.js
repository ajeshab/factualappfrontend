import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Header from '../HeaderInner';
import Footer from '../FooterInner';
export default class PesonalInformationForm extends Component {

  constructor(props) {
        
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      address: '',
      email: '',
      DOB: '',
      phone_number: '',  
      social_security_number:'',    
      driving_license_number:'',
      country:'',
      states:'',
      zip:'',
    }

    this.first_name = this.first_name.bind(this);
    this.last_name = this.last_name.bind(this);
    this.address = this.address.bind(this);
    this.email = this.email.bind(this);
    this.DOB = this.DOB.bind(this);
    this.phone_number = this.phone_number.bind(this); 
    this.social_security_number = this.social_security_number.bind(this);
    this.driving_license_number = this.driving_license_number.bind(this);
    this.country = this.country.bind(this);
    this.states = this.states.bind(this);
    this.zip = this.zip.bind(this);
    this.register = this.register.bind(this);
  }

  first_name(event) {
    this.setState({ first_name: event.target.value })
  }

  last_name(event) {
    this.setState({ last_name: event.target.value })
  }
  address(event) {
    this.setState({ address: event.target.value })
  }
 
  email(event) {
    this.setState({ email: event.target.value })
  }
  DOB(event) {
    this.setState({ DOB: event.target.value })
  }
  phone_number(event) {
    this.setState({ phone_number: event.target.value })
  }
 
  social_security_number(event) {
    this.setState({ social_security_number: event.target.value })
  }

  driving_license_number(event) {
    this.setState({ driving_license_number: event.target.value })
  }

  country(event) {
    this.setState({ country: event.target.value })
  }

  states(event) {
    this.setState({ states: event.target.value })
  }

  zip(event) {
    this.setState({ zip: event.target.value })
  }



  register(event) {
    console.log(this.state)
     fetch('http://localhost:5000/personal_info/personal_info', {
       method: 'post',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json'
       },
       body: JSON.stringify({
         first_name: this.state.first_name,
         last_name:this.state.last_name,
         address: this.state.address,
         email: this.state.email,
         DOB:this.state.DOB,
         phone_number: this.state.phone_number,
         social_security_number:this.state.social_security_number,
         driving_license_number:this.state.driving_license_number,
         country:this.state.country,
         states:this.state.states,
         zip:this.state.zip,
       })
     }).then((Response) => Response.json())
       .then((Result) => {
         if (Result.Status == 'Success')
         {
               //  this.props.history.push('/dashboard')
               // this.props.history.push(`/Login`)

                 // alert(' Registration success');
                 console.log('Registered')

         }
         else
         {

           alert('Sorrrrrry !!!! Un-authenticated User !!!!!');
         }
         })
 
   }

  
  render() {
    return (
        <div className="wrapper">
    
        <Header next="FinishedVerification"/> 
      <div class="main-wrap">
      <div class="sub-wrap">
      <div class="container" style={{backgroundColor: "#F5F5F5"}} >
      <form class="input-field col s12 m12" style={{ marginTop: "9px", padding: "0px"}}>
                  <div class="row">
                    <div class="input-field col s6 m6">
                      <input  id="first_name" type="text"
                      onChange={this.first_name} value={this.state.first_name}
                      name="first_name"
                      class="validate"
                       style={{
                        border: "1px solid gainsboro",
                        borderRadius: "13px",
                        height: "60px",
                        backgroundColor: "#fff"}}/>
                      <label for="first_name" style={{marginLeft: "20px",padding:" 0.8rem"}}>First Name</label>
                    </div>
                    <div class="input-field col s6 m6">
                      <input id="last_name" type="text" class="validate" 
                        name="last_name"
                        onChange={this.last_name} value={this.state.last_name}

                      style={{
                        border: "1px solid gainsboro",
                        borderRadius: "13px",
                        height: "60px",
                        backgroundColor: "#fff"}}/>
                      <label for="last_name"  style={{marginLeft: "20px",padding:" 0.8rem"}}>Last Name</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m12">
                      <input  id="address"
                      name="address"
                      onChange={this.address} value={this.state.address}

                      type="text" class="validate" 
                      style={{
                        border: "1px solid gainsboro",
                        borderRadius: "14px",
                        height: "75px",
                        backgroundColor: "#fff"}}/>
                      <label for="address"  style={{marginLeft: "20px",padding:" 0.8rem"}}>Physical Address</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6 m6" >
                        <input  id="email" type="text" class="validate" 
                           name="email"
                           onChange={this.email} value={this.state.email}
                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="email"  style={{marginLeft: "20px",padding:" 0.8rem"}}>Email</label>
                    </div>
                    <div class="input-field col s6 m6">
                        <input id="DOB" name="DOB"type="text" class="validate"
                          onChange={this.DOB} value={this.state.DOB}
 
                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="DOB"  style={{marginLeft: "20px",padding:" 0.8rem"}}>DOB</label>
                    </div>
                  </div>
                 <div class="row">
                    <div class="input-field col s6 m6">
                        <input  id="phone_number"
                          name="phone_number"
                          onChange={this.phone_number} value={this.state.phone_number}
                        type="text" class="validate"
                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="phone_number" style={{marginLeft: "20px",padding:" 0.8rem"}}>Phone number</label>
                    </div>
                    <div class="input-field col s6 m6">
                        <input id="social_security_number" name="social_security_number"
                    onChange={this.social_security_number} value={this.state.social_security_number}

                        type="text" class="validate"
                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="social_security_number"  style={{marginLeft: "10px",padding:" 0.8rem"}}>Social security number</label>
                    </div>
                 </div>
                  <div class="row">
                    <div class="input-field col s6 m6">
                        <input  id="driving_license_number"
                         name="driving_license_number"
                         onChange={this.driving_license_number} value={this.state.driving_license_number}

                        type="text" class="validate" 
                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="driving_license_number" style={{marginLeft: "20px",padding:" 0.8rem"}}>Driving licens number</label>
                    </div>
                    <div class="input-field col s6 m6">
                        <input id="country" name="country" type="text" class="validate"
                        onChange={this.country} value={this.state.country}

                        style={{
                            border: "1px solid gainsboro",
                            borderRadius: "13px",
                            height: "60px",
                            backgroundColor: "#fff"}}/>
                        <label for="country"  style={{marginLeft: "20px",padding:" 0.8rem"}}>Country</label>
                    </div>
                  </div>
                    <div class="row">
                        <div class="input-field col s6 m6" >
                            <input  id="states" name="states" type="text" class="validate"
                              onChange={this.states} value={this.states.state}
                            style={{
                                border: "1px solid gainsboro",
                                borderRadius: "13px",
                                height: "60px",
                                backgroundColor: "#fff"}}/>
                            <label for="states" style={{marginLeft: "20px",padding:" 0.8rem"}}>State</label>
                          </div>
                          <div class="input-field col s6 m6">
                            <input id="zip" name="zip" type="text" class="validate"
                            onChange={this.zip} value={this.state.zip}

                            style={{
                                border: "1px solid gainsboro",
                                borderRadius: "13px",
                                height: "60px",
                                backgroundColor: "#fff"}}/>
                            <label for="zip" style={{marginLeft: "20px",padding:" 0.8rem"}}>Zip</label>
                          </div>
                        </div>

                        <div class="col s12"  onClick={this.register}  style={{textAlign: "center"}}>
                        <Link to="FinishedVerification"className="input-field"  style={{width:"50.333333%",
                       display:"block",
                        borderRadius: "13px",
                        color: "#fff",
                     padding:"20px 0px",
                     margin:"0 auto",
                        backgroundColor: "#23BFFB",textAlign: "center"}}>SAVE DETAILS</Link>
                        </div> 
                </form>
                </div>
           </div>
           <Footer/>
            </div>
            </div>
    );
  }
}
