import React, { Component } from 'react';
import Header from '../HeaderInner';
import SideBar from '../SideBar';
import Footer from '../FooterInner';
export default class Dashboard extends Component {
  render() {
    return (
      <div className="wrapper">
    
      <Header/> 
    <div class="main-wrap dashboard-wrap">
<SideBar/>
      <div class="sub-wrap">
        <div class="row">
          <div class="col s6 col m3" style={{}}>
            <div
              class=""
              style={{
                display: "flex",
                borderRadius: "10px",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor:"#fff",
              }}
            >
              <a href="./Face_scan.html">
                <div class="section" style={{padding: "3rem"}}>
                  <img
                    src={require('../../assets/face_scan_web.png')}
                    style={{height: "80px", width: "80px"}}
                  />
                </div>
              </a>
            </div>
            <p style={{color: "#cacaca", textAlign: "center", fontSize: "20px"}}>
              FACE SCAN
            </p>
          </div>

          <div class="col s6 col m3" style={{}}>
            <div
              class=""
              style={{
                display: "flex",
                borderRadius: "10px",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#fff"
              }}
            >
              <a href="./Eye_scan.html">
                <div class="section" style={{padding: "3rem"}}>
                  <img
                    src={require('../../assets/Eye_scan.png')}
                    style={{height: "80px", width: "80px"}}
                  />
                </div>
              </a>
            </div>
            <p style={{color: "#cacaca", textAlign: "center", fontSize: "20px"}}>
              EYE SCAN
            </p>
          </div>

          <div class="col s6 col m3" style={{}}>
            <div
              class=""
              style={{
                display: "flex",
                borderRadius: "10px",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#fff"
              }}
            >
              <a href="./Id_front.html">
                <div class="section" style={{padding: "3rem"}}>
                  <img
                    src={require('../../assets/id.png')}
                    style={{height: "80px", width: "80px"}}
                  />
                </div>
              </a>
            </div>
            <p style={{color: "#cacaca", textAlign: "center", fontSize: "20px"}}>
              UPLOAD ID
            </p>
          </div>

          <div class="col s6 col m3" style={{}}>
            <div
              class=""
              style={{
                display: "flex",
                borderRadius: "10px",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#fff"
              }}
            >
              <a href="#">
                <div class="section" style={{padding: "3rem"}}>
                  <img
                    src={require('../../assets/security_card.png')}
                    style={{height: "80px", width: "80px"}}
                  />
                </div>
              </a>
            </div>
            <p style={{color: "#cacaca", textAlign: "center", fontSize: "20px"}}>
              SOCIAL SECURITY
            </p>
          </div>

          <div class="col s6 col m3" style={{}}>
            <div
              class=""
              style={{
                display: "flex",
                borderRadius: "10px",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#fff"
              }}
            >
              <a href="#">
                <div class="section" style={{padding: "3rem"}}>
                  <img
                    src={require('../../assets/document.png')}
                    style={{height: "80px", width: "80px"}}
                  />
                </div>
              </a>
            </div>
            <p style={{color: "#cacaca", textAlign: "center", fontSize: "20px"}}>
              OTHER DOCUMENTS
            </p>
          </div>
        </div>
      </div>
        <Footer/>
          </div>
          </div>
    );
  }
}
