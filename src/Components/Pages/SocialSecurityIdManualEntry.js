import React, { Component, useState } from 'react';
import { Link } from "react-router-dom";
import Header from '../HeaderInner';
import Footer from '../FooterInner';
import Axios from 'axios';
import Config from '../config';


export default function SocialSecurityIdManualEntry() {

  const [ssno, setSsno] = useState('')

  const handleSsno = (e) =>{
    e.preventDefault();
    if(!ssno)alert('please type your social security number');
    let payload = {
      ss_no:ssno
    }
    Axios.post(`${Config.apiRoot}/`, {...payload}).then((data) => {
      console.log('upload ssno successfull');
    }).catch((error) => {
      console.log('upload ssno error');
    })
  }
  return (
    <div className="wrapper">

      <Header next="Documents" />
      <div class="main-wrap ar-wrap">
        <div class="sub-wrap">
          <div
            style={{
              textAlign: "center",
              color: "#cacaca",
              fontSize: "22px",
              marginBottom: "30px"
            }}
          >
            Enter Social Security Number
        </div>

          <input
            id="email"
            type="email"
            class="validate"
            placeholder="Enter Social Security Number"
            onChange={(e)=>setSsno(e.target.value)}
          />
          <Link onClick={handleSsno} class="waves-effect waves-light btn-large col s6 offset-s3">ENTER</Link>
        </div>
        <Footer />
      </div>
    </div>
  )
}
