import React, { Component } from 'react';

export default class FooterInner extends Component {
  render() {
    return (
        <footer class="scanner-footer-wrap" style={{flex: 1}}>
        <div class="scanner-footer">
          Copyright 2020 IDP
        </div>
      </footer>
    );
  }
}
