import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer style={{color:"#fff", backgroundColor:"#676767", padding:"20px"}}>
      <div className="footer-copyright">
        <div className="container" style={{textAlign:"center"}}>
          © 2014 Copyright Text
        </div>
      </div>
    </footer>
    );
  }
}
