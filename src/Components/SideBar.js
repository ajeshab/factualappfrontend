import React, { Component } from 'react';
import M from "materialize-css";
export default class SideBar extends Component {
  constructor(props) {
    super(props);

  }
  componentDidMount() {
    document.addEventListener("DOMContentLoaded", function () {
      var elems = document.querySelectorAll(".sidenav");
      var instances = M.Sidenav.init(elems, {
        edge: "left",
        draggable: true,
        inDuration: 250,
        outDuration: 200,
        onOpenStart: null,
        onOpenEnd: null,
        onCloseStart: null,
        onCloseEnd: null,
        preventScrolling: true,
      });
    });
  }
  render() {
    return (
      <>
        {/* <!-- Mobile Devices--> */}
        <div
          class="row hide-on-large-only dashboard-nav"
          style={{ display: "flex", flexDirection: "row", margin: "0px" }}
        >
          <div class="col 12 s2 m2" style={{ position: "relative" }}>
            {/* <a
              href="#"
              class="sidenav-trigger"
              data-target="mobile-nav"
              style={{ color: "#3c4f58", position: "absolute", marginTop: "12px" }}
            >
              <i class="material-icons">menu</i></a> */}

            <ul
              class="sidenav"
              id="mobile-nav"
              style={{ backgroundColor: "#00bffd", padding: "63px" }}
            >
              <li>
                <a href="#">
                  <img
                    src={require('../assets/icons.png')}
                    style={{ marginLeft: "29px", height: "42px" }}
                  />
                </a>
              </li>
              <br />
              <li>
                <a href="#">
                  <img
                    src={require('../assets/settings.png')}
                    style={{ marginLeft: "29px", height: "42px" }}
                  />
                </a>
              </li>
              <br />
              <li>
                <a href="#">
                  <img
                    src={require('../assets/menu.png')}
                    style={{ marginLeft: "29px", height: "42px" }}
                  />
                </a>
              </li>
              <br />
              <li>
                <a href="#">
                  <img
                    src={require('../assets/people.png')}
                    style={{ marginLeft: "29px", height: "42px" }}
                  /><br />
                </a>
              </li>
            </ul>
          </div>

          {/* <div class="col 12 s10 m10">
      <form class="center-align">
          <input
            class="center-align"
            type="search"
            placeholder="Search"
            aria-label="Search"
            style={{
              border: "1px solid gray",
              height: "35px",
              borderRadius: "25px",
              width:"70%",
              backgroundColor: "#fff",
              marginTop: "10px",
            }}
          />
        </form> 
      </div> */}
        </div>
        {/* <!-- Large Devices--> */}



        <div class="hide-on-small-only hide-on-med-and-down side-menu">
          <img
            src={require('../assets/icons.png')}
            style={{ height: "38px", marginLeft: "18px", marginTop: "32px" }}
          /><br />

          <img
            src={require('../assets/settings.png')}
            style={{ height: "38px", marginLeft: "18px", marginTop: "32px" }}
          /><br />

          <img
            src={require('../assets/menu.png')}
            style={{ height: "38px", marginLeft: "18px", marginTop: "32px" }}
          /><br />

          <img
            src={require('../assets/people.png')}
            style={{ height: "38px", marginLeft: "18px", marginTop: "32px" }}
          />
        </div>
      </>
    );
  }
}



