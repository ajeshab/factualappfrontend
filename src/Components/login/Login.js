import React,{ useState } from 'react'
import axios from 'axios'
export default function Login() {

    const[email,setEmail] = useState('');
    const[password,setPssword] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    

    const handleSubmit = (event) => {
        event.preventDefault();
        let err = (!email && !password) ? 'email and password are required' : !email ? 'email is required' : !password ? 'password is required' :'';
            if(err){
                setError(err);
                return;
            }

            let payload = {
                user:email,
                password:password
            }
            axios.post('http:/logn', { ...payload }).then(data => {
                window.location.href = '/'
            }).catch(e=>{
                setError(e.response.data.message)
            })
       
            console.log(email, password);


    };
    
    return (
        <div style ={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            <h1  style={{color:'#234f69'}}>Login page</h1>
            {error && <div style={{color:'red'}}>{error}</div>}
            <form 
                onSubmit={handleSubmit}
                style={{border:'2px solid #004873',borderRadius:20,padding:20}}
                >
                <div>
                    <label style={{color:'#004873'}}>USER NAME :</label>
                    <input
                        type="email"
                        name="my-field-name"
                        value={email}
                        autoComplete="new-email"
                        placeholder="email/username"
                        onChange={(e)=>setEmail(e.target.value)}
                    />
                </div>
                <br/>
                <div>
                    <label style={{color:'#004873'}}>PASSWORD :</label>
                    <input
                        type="password"
                        name="password"
                        value={password}
                        autoComplete="new-password"
                        placeholder="password"
                        onChange={(e)=>setPssword(e.target.value)}
                    />
                </div>
                <br/>
                <div>
                    <button
                        type="submit"
                        placeholder="submit"
                        disabled={loading}
                        style={{color:'white',borderStyle:'hidden',backgroundColor:'#004873',borderRadius:20,padding:10}}
                        onClick={handleSubmit}
                        
                    >SUBMIT</button >
                </div>
            </form>
        </div>
    )
}
