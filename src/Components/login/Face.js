import React, { useState, useEffect, useRef } from 'react';
import * as faceapi from 'face-api.js';

export default function Face() {
    const videoHeight = 480;
    const videoWidth = 650;
    const [initializing, setInitializing] = useState(false);
    const videoRef = useRef();
    const canvasRef = useRef();

    useEffect(() => {
        const loadModels = async () => {
            const MODEL_URL = process.env.PUBLIC_URL + '/model';
            setInitializing(true);
            Promise.all([
                faceapi.nets.tinyFaceDetector.loadFromUri(MODEL_URL),
                faceapi.nets.faceLandmark68Net.loadFromUri(MODEL_URL),
                faceapi.nets.faceRecognitionNet.loadFromUri(MODEL_URL),
                faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URL),
            ]).then(startVideo);
        }
        loadModels();
    }, [])

    const startVideo = () => {
        navigator.mediaDevices.getUserMedia({
            video: {},
        }).then(stream => videoRef.current.srcObject = stream)
            .catch(console.log('error'))
    }

    const handleVideoPlay = () => {
        setInterval(async () => {
            if (initializing) {
                setInitializing(false);
            }
            canvasRef.current.innerHTML = faceapi.createCanvasFromMedia(videoRef.current);
            const displaySize = {
                height: videoHeight,
                width: videoWidth
            }
            faceapi.matchDimensions(canvasRef.current, displaySize);
            const detections = await faceapi.detectAllFaces(videoRef.current, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions();
            const resizedDetections = faceapi.resizeResults(detections, displaySize);
            canvasRef.current.getContext('2d').clearRect(0, 0, videoHeight, videoWidth);
            faceapi.draw.drawDetections(canvasRef.current, detections);
            faceapi.draw.DrawFaceLandmarks(canvasRef.current, detections);
            faceapi.draw.drawFaceExpressions(canvasRef.current, detections);

        }, 100)

    }
    return (
        <div>
            <span>{initializing ? 'initializing' : 'ready'} </span>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <video ref={videoRef} autoPlay muted height={videoHeight} width={videoWidth} onPlay={handleVideoPlay} />
                <canvas ref={canvasRef} style={{ position: "absolute" }} />
            </div>


        </div>

    )
}
