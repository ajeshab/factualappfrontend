import React, { Component } from 'react';
import M from "materialize-css";
import {Link} from "react-router-dom";
export default class HeaderInner extends Component {
  constructor(props){
    super(props)
  }
  componentDidMount()
{
    
        var elems = document.querySelectorAll(".sidenav");
        var instances = M.Sidenav.init(elems, {
          edge: "left",
          draggable: true,
          inDuration: 250,
          outDuration: 200,
          onOpenStart: null,
          onOpenEnd: null,
          onCloseStart: null,
          onCloseEnd: null,
          preventScrolling: true,
        });
   
}
  render() {
    return (
    <>
      <nav class="hide-on-small-only hide-on-med-and-down">
      <div class="nav-wrapper" style={{backgroundColor: "#fff"}}>
    
        <div class="row">
          <div class="col 12 m3">
          <Link to="/" style={{color:"#fff"}}><img   class="brand-logo-left"
                style={{width:"100px" }} src={require('../assets/IDP.png')} /></Link>

          </div>
          <div class="col 12 m3">
            <form class="center-align">
              <input
                class="center-align"
                type="search"
                placeholder="Search"
                aria-label="Search"
              
                style={{ width: "100%",border:"1px solid #ededed",borderRadius:"25px"}}
                
              />
            </form>
          </div>
        
          <div class="col 12 m3">
          <Link className="brand-logo-right" to={`${this.props.next}`} style={{color: "#000000", position: "absolute", right: "13px"}}>
             <i class="large material-icons" style={{color:" #000000"}}
              >arrow_forward
            </i>
          </Link>

          </div>
        </div>
  
      </div>
    </nav>
    <nav class="hide-on-large-only">
      <div class="nav-wrapper" style={{backgroundColor: "#fff"}}>
        <div class="row">
          <div class="col 12 m4">
          <Link to="/" style={{color:"#fff"}}><img   class="brand-logo"
              height="60"
              style={{height: "-webkit-fill-available"}} src={require('../assets/IDP.png')} /></Link>
          </div>
        </div>
        <div
          style={{float: "right",position: "relative", bottom: "16px", right: "13px"}}
        >
          <Link className="brand-logo-right" to={`${this.props.next}`} style={{color:"#fff"}}>
             <i class="large material-icons" style={{color:" #000000"}}
              >arrow_forward
            </i>
          </Link>
      
        </div>
      </div>
    </nav>

    <ul class="sidenav" id="mobile-nav" style={{padding: "50px"}}>
    <li><Link to="/"  style={{color:"black"}}>Home</Link></li>

<li> <Link to="Agreement"  style={{color:"black"}}>New user</Link></li>
<li>
<Link to="FaceScan"  style={{color:"black"}}>Existing user</Link>
</li>
    </ul>
    <div
      class="row hide-on-large-only"
      style={{display:"flex", flexDirection:"row", margin: "0px"}}
    >
      <div class="col 12 s2 m2" style={{position: "relative",}}>
        <a
          href="#"
          class="sidenav-trigger"
          data-target="mobile-nav"
          style={{color: "#3c4f58", position: "absolute", marginTop: "12px"}}
        >
          <i class="material-icons">menu</i></a >
      </div>
      <div class="col 12 s10 m10">
        <form class="center-align">
          <input
            class="center-align"
            type="search"
            placeholder="Search"
            aria-label="Search"
            style={{
              border: "1px solid gray",
              height: "35px",
              borderRadius: "25px",
              width:"70%",
              backgroundColor: "#fff",
              marginTop: "10px",
            }}
          />
        </form>
      </div>
    </div>
</>
    );
  }
}
