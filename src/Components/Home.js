import React, { Component } from 'react';
import M from "materialize-css";
import Header from './Header';
import Footer from './Footer';
export default class Home extends Component {

  constructor(props) {
    super(props);
 
  }
    componentDidMount(){
     
        var elems = document.querySelectorAll(".carousel");
        var instances = M.Carousel.init(elems, { indicators: true });
      
        // M.AutoInit();

       
      }

    render(){
        return <>
         <Header/>
        <div
      className="container hide-on-small-only hide-on-med-and-down"
      style={{display: "flex",justifyContent: "space-around",marginTop: "14px"}}
    >
      <div
        style={{ width: "100%",
            textAlign: "center",
            fontSize: "30px",
            position: "relative",
            }}
       
      >
      <div
          style={{
            alignItems: "center",
            justifySelf: "center",
            marginTop: "100px",
            fontSize: "30px",
            fontWeight: "600",
            color: "#333333",
          }}
        >
         IDP VERIFICATION
          <p style={{ fontSize: "20px",
            fontWeight:"400",
            color: "#909090",}}>For Complete Secure Solution</p>
        </div>
      </div>
      <div
        className=""
        style={{ width:"100%",
            textAlign:"center",
            fontSize:"30px",
            position: "relative",
            }}
      >
        <img
          className="responsive-img"
          style={{width:"350px", height:"350px", float:"right"}}
          src={require('../assets/Rectangle 2.png')}
        />
      </div>
    </div>
{/* Small Devices */}

    <div
      className="container hide-on-large-only"
      style={{display: "flex", flexDirection: "column"}}
    >
      <div style={{width: "100%", textAlign: "center"}}>
        <div
          style={{
            alignItems: "center",
            justifySelf: "center",
            marginTop: "100px",
            fontSize: "30px",
            fontWeight: "600",
            color: "#333333",
          }}
        >
          <p>For Complete Secure Solution</p>
          IDP VERIFICATION
        </div>

      
      </div>
      <div className="" style={{width: "100%", textAlign: "center"}}>
        <img
          className="responsive-img"
          style={{width: "350px", height: "350px"}}
          src={require('../assets/Rectangle 2.png')}
        />
      </div>
    </div>


    <div className="row">
      <div className="col s12 col m4" style={{textAlign: "center"}}>
        <img
          src={require('../assets/about.png')}
          style={{height: "65px", width: "65px"}}
        />
        <div className="hide-on-small-only" style={{fontSize: "2rem"}}>ABOUT</div>
        <h5 className="show-on-small hide-on-large-only hide-on-med-and-down">
          ABOUT
        </h5>
        <p style={{margin: "20px"}}>
          position or rank in relation to others the status of a father.
          relative rank in a hierarchy of prestige especially high prestige. the
          condition of a person or thing in the eyes of the law. state or
          condition with respect to circumstances the status of the negotiations
        </p>
      </div>

      <div className="col s12 col m4" style={{textAlign: "center"}}>
        <img
          src={require('../assets/vision.png')}
          style={{height: "65px", width: "65px"}}
        />
        <div className="hide-on-small-only" style={{fontSize: "2rem"}}>VISION</div>
        <h5 className="show-on-small hide-on-large-only hide-on-med-and-down">
          VISION
        </h5>
        <p style={{margin: "20px"}}>
          position or rank in relation to others the status of a father.
          relative rank in a hierarchy of prestige especially high prestige. the
          condition of a person or thing in the eyes of the law. state or
          condition with respect to circumstances the status of the negotiations
        </p>
      </div>

      <div className="col s12 col m4" style={{textAlign: "center"}}>
        <img
          src={require('../assets/services.png')}
          style={{height: "65px", width: "65px"}}
        />
        <div className="hide-on-small-only" style={{fontSize: "2rem"}}>SERVEICS</div>
        <h5 className="show-on-small hide-on-large-only hide-on-med-and-down">
          SERVEICS
        </h5>
        <p style={{margin: "20px"}}>
          position or rank in relation to others the status of a father.
          relative rank in a hierarchy of prestige especially high prestige. the
          condition of a person or thing in the eyes of the law. state or
          condition with respect to circumstances the status of the negotiations
        </p>
      </div>
    </div>


    <div style={{backgroundColor:"#00bffd", marginBottom:"10px"}}>
      <div
        className="hide-on-med-and-down"
        style={{
            color: "#fff",
            textAlign: "center",
            fontSize: "25px",
            fontWeight: "700"}}
      >
        OUR SERVICES
      </div>
      <div
        className="hide-on-large-only"
        style={{
          textAlign: "center",
          color: "#fff",
          fontSize: "19px",
          fontWeight: "700"
        }}
      >
        OUR SERVICES
      </div>
      <div style={{textAlign:"center", alignItems: "center", margin: "15px"}}>
        <p
          style={{
            color: "#fff",
            textAlign: "center",
            marginLeft: "10%",
            marginRight: "10%"}}
        >
          position or rank in relation to others the status of a father.
          relative rank in a hierarchy of prestige especially high prestige. the
          condition of a person or thing in the eyes of the law. state or
          condition with respect to circumstances the status of the negotiations
          the condition of a person or thing in the eyes of the law. state or
          condition with respect to circumstances the status of the negotiations
        </p>
      </div>
      <br />
      <div
        className="center-align hide-on-large-only"
        style={{
            backgroundColor:"#fff",
            height:"40px",
            width:"30%",
            margin:"auto",
            padding:"10px",
            borderRadius:"20px",
            textAlign:"center",
            alignItem:"center"
        }}
      >
        <a  href="#" style={{color: "#00bffd", textAlign: "center"}}
          >Visit Website</a
        >
      </div>

      <div
        className="center-align hide-on-med-and-down"
        style={{
            backgroundColor:"#fff",
            height:"40px",
            width:"19%",
            margin:"auto",
            padding:"10px",
            borderRadius:"20px",
            textAlign:"center",
            alignItem:"center"
        }}
      >
        <a href="#" style={{color: "#00bffd", textAlign: "center"}}
          >Visit Website</a
        >
      </div>
      <br />
    </div>



    <div className="carousel">
      <a className="carousel-item" href="#one!"
        ><img src={require('../assets/face_scan_web.png')}/>
        <p  style={{color:"#cacaca", textAlign:"center", fontSize: "20px"}}>
          FACE SCAN
        </p>
      </a>
      <a className="carousel-item" href="#three!"
        ><img src={require('../assets/id.png')} />
        <p  style={{color:"#cacaca", textAlign:"center", fontSize: "20px"}}>
          ID VERIFICATION
        </p></a
      >
      <a className="carousel-item" href="#four!"
        ><img src={require('../assets/security_card.png')} />
        <p  style={{color:"#cacaca", textAlign:"center", fontSize: "20px"}}>
          SOCIALSECURITY CARD VERIFICATION
        </p></a
      >
      <a className="carousel-item" href="#five!">
          <img src={require('../assets/document.png')} />
        <p style={{color:"#cacaca", textAlign:"center", fontSize: "20px"}}>
          DOCUMENT VERIFICATION
        </p></a >
    </div>
    <Footer/>
        </>
   
  }
}
