import React, { Component } from 'react';
import M from "materialize-css";
import {Link} from "react-router-dom";

export default class Header extends Component {
  constructor(props){
    super(props)
  }
  componentDidMount()
{
  
        var elems = document.querySelectorAll(".sidenav");
        var instances = M.Sidenav.init(elems, {
          edge: "left",
          draggable: true,
          inDuration: 250,
          outDuration: 200,
          onOpenStart: null,
          onOpenEnd: null,
          onCloseStart: null,
          onCloseEnd: null,
          preventScrolling: true,
        });
    
}
  render() {
    return (
      <>
      
      <div
      className="hide-on-small-only hide-on-med-and-down"
      style={{backgroundColor:"#00bffd",
          height:"31px",
          color:"#fff",
           display:"flex",
          flexDirection:"row",
           justifyContent:"space-around"}}
       >
      <div
        className="col s12 col m4"
        style={{
          color:"#fff",
          marginLeft:"20px",
          fontSize:"14px",
          padding:"5px"}}
        
      >
        identitypropd@gmail.com
      </div>

      <div
        className="col s12 col m4"
        style={{
          color:"#fff",
          position:"relative",
          right:"7%",
          fontSize:"14px",
          padding:"5px"}}
      >
        0123894567
      </div>
      <div
        className="col s12 col m4"
        style={{
          color:"#fff",
          position:"relative",
          left:"30%",
          fontSize:"14px",
          padding:"5px"}}
     >
       <Link to="Agreement" style={{color:"#fff"}}>New user</Link>
        {/* <a href="/Agreement" style={{color:"#fff"}}>New user</a> */}
      </div>

      <div
        className="col s12 col m4 right-align"
        style={{
          color: "#fff",
          float: "right",
          right: "20px",
          fontSize:"14px",
          padding:"5px",}}
      >
         <Link to="FaceScan" style={{color:"#fff"}}>Existing user</Link>
        
      </div>
    </div>

  <div className="head">
    <nav>
      <div className="nav-wrapper" style={{backgroundColor:"#ffffff"}}>
      <Link to="/" style={{color:"#fff"}}><img style={{width:"100px",height:"55px"}} src={require('../assets/IDP.png')} /></Link>


        <a href="#" className="sidenav-trigger" data-target="mobile-nav">
          <i className="material-icons" style={{color:"black"}}>menu</i>
        </a>

        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li><a href="#" style={{color:"black"}}>HOME</a></li>
          <li><a href="#"style={{color:"black"}}>TERMS & CONDITIONS</a></li>
          {/* <li><a href="#"style={{color:"black"}}>MORE</a></li> */}
          {/* <li><a href="#" style={{color:"black"}}>ABOUT</a></li>
          <li><a href="#" style={{color:"black"}}>NEWS</a></li> */}
        </ul>
      </div>
    </nav>
    <ul className="sidenav" id="mobile-nav" style={{padding:"50px"}}>
      <li><Link to="/"  style={{color:"black"}}>Home</Link></li>

      <li> <Link to="Agreement"  style={{color:"black"}}>New user</Link></li>
      <li>
      <Link to="FaceScan"  style={{color:"black"}}>Existing user</Link>
      </li>
    </ul>
</div>

  

</>
    );
  }
}
